package s1.demo

import javax.imageio.ImageIO
import java.io.File
import s1.image.ImageExtensions._
import scala.math._

object Circles2 extends Effect(800, 800){

  var clock = 0
  var pic = emptyImage
  val textPic = ImageIO.read(new File("pictures/merrychristmas.png"))

  //green
  val color2 = s1.image.Color(48,54,15)
  //red
  val color1 = s1.image.Color(203,65,43)

  val focalPointWidth = 250
  val focalPointHeight = 250
  val centerTranslate = 400
  val angularFreq = 3
  val angularPhase = scala.math.Pi / 4.0

  val ringWidth = 5

  val animatingSpeed = 8
  //animate two focal points with lissajous curves
  //equation:
  //x = Asin(wt+s) + centertranslate
  //y = sin(t) + centertranslate
  var fx1 = focalPointWidth * scala.math.sin(angularFreq*clock+angularPhase) + centerTranslate
  var fy1 = scala.math.sin(clock)*focalPointHeight + centerTranslate

  var fx2 = focalPointWidth * scala.math.sin(angularFreq*clock+angularPhase+angularPhase/2.0) + centerTranslate
  var fy2 = scala.math.sin(clock+scala.math.Pi)*focalPointHeight + centerTranslate

  def calculatePoints() = {
    val nextImage = pic.copy

    for(y <- 1 until this.width){
      for(x <- 1 until this.height){
        //calculate distances from both points
        val distance1 = scala.math.round(distanceBetweenPoints(x,y,fx1,fy1)).toInt
        val distance2 = scala.math.round(distanceBetweenPoints(x,y,fx2,fy2)).toInt
        //calculate the color for each value using bitwise operations
        if (((distance1 ^ distance2) >> ringWidth & 1) % 2 == 0) nextImage(x,y).color = color1 else nextImage(x,y).color = color2
      }
    }
    nextImage
  }

  def distanceBetweenPoints(x1: Double,y1: Double,x2: Double,y2: Double) = {
    sqrt(pow(x1-x2,2) + pow(y1-y2, 2))
  }

  def makePic() = {
    pic = calculatePoints()
    //shift the image based on time
    pic.graphics.drawImage(textPic, clock*3-1000, 350, null)
    pic
  }

  def changeThings() = {
    //coordinate clock used to calculate the sin values
    val coordinateClock = clock / (animatingSpeed*10.0)
    //shift the values of the focal points based on the clock
    fx1 = focalPointWidth * scala.math.sin(angularFreq*coordinateClock+angularPhase) + centerTranslate
    fy1 = scala.math.sin(coordinateClock)*focalPointHeight + centerTranslate
    fx2 = focalPointWidth * scala.math.sin(angularFreq*coordinateClock+angularPhase+angularPhase/2.0) + centerTranslate
    fy2 = scala.math.sin(coordinateClock+scala.math.Pi)*focalPointHeight + centerTranslate
    clock += 1
  }

  def next = clock > 400

}
