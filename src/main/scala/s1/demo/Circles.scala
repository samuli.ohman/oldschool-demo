package s1.demo

import javax.imageio.ImageIO
import java.io.File
import s1.image.ImageExtensions._

import scala.collection.mutable.Buffer
import scala.math._


object Circles extends Effect(800, 800) {
  // This variable is used to find out when to end this effect
  val animatingSpeed = 10
  val lineAmount = 250
  val radius = 400
  val increase = 0.0012*animatingSpeed

  var clock = 0
  var currentColor = 0.0
  var coefficient = 0.0
  class Line(val x1: Int, val y1: Int, val x2: Int, val y2: Int)

  // A completely random color used later in the algorithm
  val transparent = s1.image.Color(0,0,0,0)

  // Clamp used to keep (randomly generated) pixel coordinates inside the image area
  def clamp(v: Int, maxValue: Int) = math.max(0, math.min(v, maxValue))

  /**
   * This method creates (or in this case only returns) a BufferedImage
   * of the effect
   */
  def makePic() = {
    // Get an empty space where to draw
    val pic = emptyImage

    // Get the tools to draw with
    val graphics = pic.graphics

    //set bg color
    graphics.setColor(new java.awt.Color(204,151,100))
    graphics.fillRect(0,0, this.width, this.height)

    //fade between two colors
    val nextColor = new java.awt.Color(
      (103+scala.math.sin(currentColor)*83).toInt,
      (55-scala.math.sin(currentColor)*35).toInt,
      (49-scala.math.sin(currentColor)*9).toInt,
    )

    val lines = getLines(clock)

    // And then use it to fill an oval
    graphics.setColor(nextColor)
    lines.foreach(line => graphics.drawLine(line.x1, line.y1, line.x2, line.y2))

    // Finally we return the picture we created.
    pic
  }

  def getLines(clock: Int):Vector[Line] = {
    val middle = (width/2,height/2)
    val lines = Buffer[Line]()

    //Calculate start and end position for each line
    for(i <- 0 to lineAmount){
      val angle = Pi + 1.0 * i / lineAmount * 2 * Pi
      val angle2 = Pi + 1.0 * i * coefficient / lineAmount * 2 * Pi

      lines += new Line(middle._1-(radius*cos(angle)).toInt, middle._2-(radius*sin(angle)).toInt,
        middle._1+(radius*cos(angle2)).toInt, middle._2+(radius*sin(angle2)).toInt)
    }
    lines.toVector
  }

  /**
   * This method updates the state of the effect - In this particular algorithm it
   * actually manipulates the image.
   */
  def changeThings() = {
    if(coefficient < 2.5){
      coefficient += increase
    }else{
      coefficient += increase*2.5
    }

    currentColor += 0.1
    if(coefficient >= 40)
    // This helper variable is used to check when to change effects
    clock += 1
  }

  // The effect ends when we have changed the model 300 times
  def next = clock > 500

}
