A-ryhmä

Nimet:
Aapo Leppänen (SuperAapo)
Samuli Öhmän (löhmä)

Efekti: _varying circles_

- Samuli teki projetkin pohjan ja _circles_ effekting
- Aapo lisäsi värit demoihin ja teki _circles2_ efekting jossa oli teksti

Circles2 efektissä luotiin kaksi pistettä jotka liikuivat lissajous'n käyriä käyttäen. Sen jälkeen jokaiselle koordinaatille laskettiin pituus molempiin pisteisiin ja käyttäen XOR operaatiota väritettiin pisteet joko värillä 1 tai 2.


Inspiraatiota:
https://seancode.com/demofx/
